import { Test, TestingModule } from '@nestjs/testing';
import { EccooLogger } from './eccoo-logger';

describe('EccooLogger', () => {
  let provider: EccooLogger;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EccooLogger],
    }).compile();

    provider = await module.resolve<EccooLogger>(EccooLogger);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
