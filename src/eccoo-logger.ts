import { ConsoleLogger, Injectable, Scope } from '@nestjs/common';
import * as BunyanLogger from 'bunyan';

@Injectable({ scope: Scope.TRANSIENT })
export class EccooLogger extends ConsoleLogger {
  private logger: BunyanLogger;

  constructor(context?: string) {
    super();
    this.context = context;
    this.logger = BunyanLogger.createLogger({
      name: context || 'root',
      src: false,
      level: BunyanLogger.DEBUG,
    });
  }

  log(message: string, context?: string) {
    this.logger.info({ context: context || this.context }, message);
  }

  error(message: string, trace?: string, context?: string) {
    const err = new Error(message);
    err.stack = trace;
    this.logger.error({
      context: context || this.context,
      err,
    });
  }

  warn(message: string, context?: string) {
    this.logger.warn(
      {
        context: context || this.context,
      },
      message,
    );
  }

  debug(message: string, context?: string) {
    this.logger.debug(
      {
        context: context || this.context,
      },
      message,
    );
  }

  verbose(message: string, context?: string) {
    this.logger.trace(
      {
        context: context || this.context,
      },
      message,
    );
  }
}
