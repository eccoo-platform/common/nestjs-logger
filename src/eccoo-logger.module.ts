import { Module } from '@nestjs/common';
import { EccooLogger } from './eccoo-logger';

@Module({
  providers: [EccooLogger],
  exports: [EccooLogger],
})
export class EccooLoggerModule {}
