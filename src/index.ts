import { EccooLoggerModule } from './eccoo-logger.module';
import { EccooLogger } from './eccoo-logger';

export { EccooLoggerModule, EccooLogger };
