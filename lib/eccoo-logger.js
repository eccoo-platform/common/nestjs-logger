"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EccooLogger = void 0;
const common_1 = require("@nestjs/common");
const BunyanLogger = require("bunyan");
let EccooLogger = class EccooLogger extends common_1.ConsoleLogger {
    constructor(context) {
        super();
        this.context = context;
        this.logger = BunyanLogger.createLogger({
            name: context || 'root',
            src: false,
            level: BunyanLogger.DEBUG,
        });
    }
    log(message, context) {
        this.logger.info({ context: context || this.context }, message);
    }
    error(message, trace, context) {
        const err = new Error(message);
        err.stack = trace;
        this.logger.error({
            context: context || this.context,
            err,
        });
    }
    warn(message, context) {
        this.logger.warn({
            context: context || this.context,
        }, message);
    }
    debug(message, context) {
        this.logger.debug({
            context: context || this.context,
        }, message);
    }
    verbose(message, context) {
        this.logger.trace({
            context: context || this.context,
        }, message);
    }
};
EccooLogger = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.TRANSIENT }),
    __metadata("design:paramtypes", [String])
], EccooLogger);
exports.EccooLogger = EccooLogger;
