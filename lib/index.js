"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EccooLogger = exports.EccooLoggerModule = void 0;
const eccoo_logger_module_1 = require("./eccoo-logger.module");
Object.defineProperty(exports, "EccooLoggerModule", { enumerable: true, get: function () { return eccoo_logger_module_1.EccooLoggerModule; } });
const eccoo_logger_1 = require("./eccoo-logger");
Object.defineProperty(exports, "EccooLogger", { enumerable: true, get: function () { return eccoo_logger_1.EccooLogger; } });
